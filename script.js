
// 1. *Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}.`);


//OK
// 2. Create a variable address with a value of an array containing details of an address. Destructure the array and print out a message with the full address using Template Literals.
let address = ['258 Washington Avenue', 'NW California', 'USA'];
let [street, city, country] = address;
console.log(`I live in ${street}, ${city}, ${country}`);


//OK
// 3.  Create a variable animal with a value of an object data type with different animal details as it’s properties.

let animal = { name: 'Lolong', type: 'saltwater crocodile', weight: 1075, size:"20 ft 3 in"};
let { name, type, weight, size } = animal;
console.log(`${name} is a ${type}. He weighed ${weight} kgs with a measurement of ${size}`);

//OK
// 4. Create an array of numbers. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
const numberArray = [1, 2, 3, 4, 5];
numberArray.forEach((num) => console.log(num));


//OK
// 5. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const reduceNumber = numberArray.reduce((num, sum) => sum + num, 0);
console.log(reduceNumber);


//OK
// 6. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// Create/instantiate a new object from the class Dog and console log the object.

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let dog1 = new Dog('Maui', 1, 'American Bully');
let dog2 = new Dog('Timmy', 2, 'Aspin');
let dog3 = new Dog('Momo', 4, 'Golden Retriever');

console.log(dog1);
console.log(dog2);
console.log(dog3);

//OK